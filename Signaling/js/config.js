/* eslint-disable no-unused-vars */

// Make a copy of this file and save it as config.js (in the js directory).

// Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
// Do not include the trailing slash. See the README for more information:

var SAMPLE_SERVER_BASE_URL = 'https://evening-sierra-48561.herokuapp.com/';

// OR, if you have not set up a web server that runs the learning-opentok-php code,
// set these values to OpenTok API key, a valid session ID, and a token for the session.
// For test purposes, you can obtain these from https://tokbox.com/account.

var API_KEY = '47178634';
var SESSION_ID = '1_MX40NzE3ODYzNH5-MTYxNzEwODMyMjIyNn5BWElXV0NwcmNIWmFpNndqUWJ0bW8rajd-fg';
var TOKEN = 'T1==cGFydG5lcl9pZD00NzE3ODYzNCZzaWc9YzVmNGNlZmJjNWMyZjVlYjVkZTc4Mzk0MmNmZTdlYWFiMjkyZmJhYzpzZXNzaW9uX2lkPTFfTVg0ME56RTNPRFl6Tkg1LU1UWXhOekV3T0RNeU1qSXlObjVCV0VsWFYwTndjbU5JV21GcE5uZHFVV0owYlc4cmFqZC1mZyZjcmVhdGVfdGltZT0xNjE3MTA4MzMzJm5vbmNlPTAuMzA4NDEyMzY1MDc3Njkmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTYxOTcwMDMzMyZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==';
